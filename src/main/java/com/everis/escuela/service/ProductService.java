package com.everis.escuela.service;

import java.util.List;

import com.everis.escuela.entity.Product;

public interface ProductService {

	public List<Product> listAll();

	public Product findById(Long id);

	public Product save(Product product);
}
